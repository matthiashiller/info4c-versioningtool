# Versioning Tool Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## 1.0.2 - 2020-05-07
### Added
- Hotfix permissions for admin

## 1.0.1 - 2020-05-07
### Added
- Initial release
