<?php

namespace info4c\versioningtool\services;

use Craft;
use craft\base\Component;

use craft\elements\Asset;
use craft\helpers\ArrayHelper;
use craft\helpers\DateTimeHelper;
use craft\helpers\FileHelper;

use info4c\versioningtool\helpers\Media;
use info4c\versioningtool\helpers\SpreadsheetHelper;
use info4c\versioningtool\records\ListRecord;

class ListService extends Component
{

    /**
     * @param $listType
     *
     * @return array|null[]
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function listStatus($listType): array
    {

        $this->_cleanupAssets($listType);

        // search last record of given list type
        $listRecord = ListRecord::find()
            ->where(['listType' => $listType])
            ->orderBy('published DESC, dateCreated DESC')
            ->one();

        if ($listRecord) {
            // if the exported list exists -> return data of last commit
            if ($listRecord->getAttribute('assetVersioned')) {
                $asset = Asset::find()
                    ->id($listRecord->getAttribute('assetVersioned'))
                    ->one();
                $assetForeign = Asset::find()
                    ->id($listRecord->getAttribute('assetVersioning'))
                    ->one();
                $published = DateTimeHelper::toDateTime($listRecord->getAttribute('published'));
                return [
                    'listUid' => $listRecord->uid,
                    'assetUid' => $asset->uid,
                    'assetName' => $asset->filename,
                    'assetForeignUid' => $assetForeign->uid,
                    'lastCommit' => $published->format('d.m.Y'),
                ];
            }
        }

        return [
            'listUid' => null,
            'assetUid' => null,
            'assetName' => null,
            'assetForeignUid' => null,
            'lastCommit' => null,
        ];
    }


    /**
     * @param $uid
     *
     * @return array
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function resetList($uid): array
    {
        $listRecord = ListRecord::find()
            ->where(['uid' => $uid])
            ->one();

        $elements = Craft::$app->getElements();
        $attributes = ['assetVersioning', 'assetVersioned', 'assetOrigin'];
        foreach ($attributes as $attr) {
            $id = $listRecord->getAttribute($attr);
            if ($id) {
                $elements->deleteElementById($id, null, null, true);
            }
        }

        $listType = $listRecord->getAttribute('listType');
        $listRecord->delete();

        return $this->listStatus($listType);
    }

    /**
     * @param $file
     * @param $uid
     *
     * @return array
     * @throws \craft\errors\AssetLogicException
     * @throws \craft\errors\FileException
     * @throws \Exception
     */
    public function replaceListAsset($file, $uid): array
    {
        // the record which $file belongs to
        $listRecord = ListRecord::find()
            ->where(['uid' => $uid])
            ->one();

        $published = DateTimeHelper::toDateTime($listRecord->getAttribute('published'));

        $versionedAsset = Asset::find()
            ->id($listRecord->getAttribute('assetVersioned'))
            ->one();

        $assets = Craft::$app->getAssets();
        $assets->replaceAssetFile($versionedAsset, $file['tmp_name'], $file['name']);

        $folder = Media::getListFolder('versionings', $listRecord->getAttribute('listType'));
        $volume = Craft::$app->getVolumes()->getVolumeById($folder->volumeId);
        $versionedFile = Craft::getAlias($volume->path.DIRECTORY_SEPARATOR.$versionedAsset->path);

        $originArray = SpreadsheetHelper::spreadsheetToArray($versionedFile, 0, 'strPersonID');

        $assetOrigin = Asset::find()
            ->id($listRecord->getAttribute('assetOrigin'))
            ->one();

        $originArray = $this->_updateOriginList($originArray, $assetOrigin, $listRecord->listType);

        $tempPath = Craft::$app->getPath()->getTempPath();
        $originFile = $tempPath.DIRECTORY_SEPARATOR.$assetOrigin->filename;
        FileHelper::writeToFile($originFile, json_encode($originArray, JSON_THROW_ON_ERROR, 512));

        $assets->replaceAssetFile($assetOrigin, $originFile, $assetOrigin->filename);

        return [
            'listUid' => $listRecord->getAttribute('uid'),
            'assetUid' => $versionedAsset->uid,
            'assetName' => $versionedAsset->filename,
            'lastCommit' => $published->format('d.m.Y'),
        ];

    }

    /**
     * @param $listType
     *
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    private function _cleanupAssets($listType)
    {
        $listRecord = ListRecord::find()
            ->where(['listType' => $listType, 'assetVersioned' => null])
            ->all();

        if ($listRecord) {
            $elements = Craft::$app->getElements();
            foreach ($listRecord as $record) {
                $assetForeign = Asset::find()
                    ->id($record->getAttribute('assetVersioning'))
                    ->one();
                if ($assetForeign) {
                    $elements->deleteElement($assetForeign, true);
                }
                $assetOrigin = Asset::find()
                    ->id($record->getAttribute('assetOrigin'))
                    ->one();
                if($assetOrigin) {
                    $elements->deleteElement($assetOrigin, true);
                }
                $record->delete();
            }
        }

    }

    /**
     * @param       $currentList
     * @param Asset $previousOrigin
     * @param       $listType
     *
     * @return mixed
     * @throws \JsonException
     */
    private function _updateOriginList($currentList, Asset  $previousOrigin, $listType)
    {
        $folder = Media::getListFolder('versionings', $listType);
        $volume = Craft::$app->getVolumes()->getVolumeById($folder->volumeId);
        $file = Craft::getAlias($volume->path.'/'.$previousOrigin->path);
        $file = file_get_contents($file);
        $prevOrigin = ArrayHelper::index(json_decode($file, true, 512, JSON_THROW_ON_ERROR), 'strPersonID');

        foreach ($currentList as &$row) {
            $row['dataKey'] = $prevOrigin[$row['strPersonID']]['dataKey'];
        }
        unset($row);

        return $currentList;
    }

}
