<?php

namespace info4c\versioningtool\services;

use Craft;
use craft\base\Component;
use craft\helpers\ArrayHelper;
use craft\helpers\FileHelper;
use craft\elements\Asset;
use craft\helpers\DateTimeHelper;

use info4c\versioningtool\models\OriginListModel;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Url;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

use info4c\versioningtool\records\ListRecord;
use info4c\versioningtool\helpers\Media;

class CreateListService extends Component
{

    protected $folder;
    protected $published;
    protected $previousOrigin = null;
    protected $listOrigin = [];

    /**
     * @param array $params
     *
     * @return array
     * @throws \JsonException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Throwable
     * @throws \craft\errors\AssetLogicException
     * @throws \craft\errors\ElementNotFoundException
     * @throws \craft\errors\FileException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     */
    public function createList(array $params): array
    {
        $this->folder = Media::getListFolder('versionings', $params['listType']);
        $volume = Craft::$app->getVolumes()->getVolumeById($this->folder->volumeId);
        $tempPath = Craft::$app->getPath()->getTempPath();

        $listRecord = ListRecord::find()
            ->where(['listType' => $params['listType']])
            ->orderBy('published DESC, dateCreated DESC')
            ->one();

        $this->published = DateTimeHelper::toDateTime($listRecord->getAttribute('published'));

        // update the origin JSON file
        $originList = Asset::find()
            ->id($listRecord->getAttribute('assetOrigin'))
            ->one();
        $originList  = Craft::getAlias($volume->path.'/'.$originList->path);
        $this->_updateOriginList($originList, $params);
        $originFile = $tempPath.DIRECTORY_SEPARATOR.$this->published->format('Y-m-d-').'origin.json';
        FileHelper::writeToFile($originFile, json_encode($this->listOrigin, JSON_THROW_ON_ERROR, 512));
        $this->_updateAssetRecord($listRecord, 'assetOrigin', $originFile);

        // generate excel file for export
        $excelFile = $tempPath.DIRECTORY_SEPARATOR.$this->published->format('Y-m-d-').'freeze-list.xlsx';
        $this->_createOriginExcel($params, $excelFile);
        $assetUid = $this->_updateAssetRecord($listRecord, 'assetVersioned', $excelFile);

        $listRecord->save();

        return ['success' => true, 'message' => 'happy?', 'assetUid' => $assetUid];
    }


    /**
     * @param Asset $file
     * @param       $params
     *
     * @throws \JsonException
     */
    private function _updateOriginList($file, $params): void
    {
        $originList = file_get_contents($file);
        $originList = json_decode($originList, true, 512, JSON_THROW_ON_ERROR);
        ksort($originList);

        $originIndexed = ArrayHelper::index($originList, 'strPersonID');
        ksort($originIndexed);

        $mutations = json_decode($params['mutations'], true, 512, JSON_THROW_ON_ERROR);
        foreach ($mutations as $mutation) {
            $originIndexed[$mutation['strPersonID']] = array_merge($originIndexed[$mutation['strPersonID']], $mutation);
        }

        $new = json_decode($params['new'], true, 512, JSON_THROW_ON_ERROR);
        foreach ($new as $item) {
            $originIndexed[$item['strPersonID']] = array_merge($originIndexed[$item['strPersonID']], $item);
        }

        foreach ($originIndexed as $value) {
            $this->listOrigin[] = $value;
        }

    }

    /**
     * @param ListRecord $record
     * @param            $attribute
     * @param            $transferFile
     *
     * @return array|string
     * @throws \Throwable
     * @throws \craft\errors\AssetLogicException
     * @throws \craft\errors\ElementNotFoundException
     * @throws \craft\errors\FileException
     * @throws \yii\base\Exception
     */
    private function _updateAssetRecord(ListRecord $record, $attribute, $transferFile)
    {
        $recAttr = $record->getAttribute($attribute);
        if (!$recAttr) {
            $asset = Media::getNewAssetFromFile($transferFile, $this->folder);
            $result = Craft::$app->getElements()->saveElement($asset);
            if ($result) {
                $record->setAttribute($attribute, $asset->id);
            } else {
                return ['success' => false, 'errors' => $asset->getErrors()];
            }
        } else {
            $asset = Asset::find()
                ->id($recAttr)
                ->one();
            $assets = Craft::$app->getAssets();
            $assets->replaceAssetFile($asset, $transferFile, basename($transferFile));
        }
        return $asset->uid;
    }


    /**
     * @param array  $params
     * @param string $file
     *
     * @return string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \JsonException
     */
    private function _createOriginExcel(array $params, string $file)
    {

        $fields = new OriginListModel();
        $fieldsOrder = $fields->attributes();

        $fullList = $this->_sheetByFieldsOrder($this->listOrigin, $fieldsOrder, true);

        $mutations = json_decode($params['mutations'], true, 512, JSON_THROW_ON_ERROR);
        $mutationList = $this->_sheetByFieldsOrder($mutations, $fieldsOrder);

        $newItems = json_decode($params['new'], true, 512, JSON_THROW_ON_ERROR);
        $newList = $this->_sheetByFieldsOrder($newItems, $fieldsOrder);

        $deletedItems = json_decode($params['deleted'], true, 512, JSON_THROW_ON_ERROR);
        $deletedList = $this->_sheetByFieldsOrder($deletedItems, $fieldsOrder);

        $spreadsheet = new Spreadsheet();

        $spreadsheet->setActiveSheetIndex(0)
            ->fromArray($fieldsOrder, '', 'A1')
            ->fromArray($fullList, '', 'A2');
        $spreadsheet->getActiveSheet()->setTitle('full list ('.count($fullList).')');

        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(1)
            ->fromArray($fieldsOrder, '', 'A1')
            ->fromArray($mutationList, '', 'A2');
        $spreadsheet->getActiveSheet()->setTitle('mutations ('.count($mutationList).')');

        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(2)
            ->fromArray($fieldsOrder, '', 'A1')
            ->fromArray($newList, '', 'A2');
        $spreadsheet->getActiveSheet()->setTitle('new ('.count($newList).')');

        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(3)
            ->fromArray($fieldsOrder, '', 'A1')
            ->fromArray($deletedList, '', 'A2');
        $spreadsheet->getActiveSheet()->setTitle('deleted ('.count($deletedList).')');

        $spreadsheet->setActiveSheetIndex(0);

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($file);
    }

    /**
     * @param array $list
     * @param array $fields
     *
     * @param bool  $publishDate
     *
     * @return array
     */
    private function _sheetByFieldsOrder(array $list, array $fields, $publishDate = false): array
    {
        $sheet = [];
        foreach ($list as $row) {
            if ($publishDate) {
                $row['strDateOfPublicationOfTheList'] = $this->published->format('d.m.Y');
            }
            $toPush = [];
            foreach ($fields as $field) {
                $toPush[$field] = $row[$field];
            }
            $sheet[] = $toPush;
        }

        return $sheet;
    }

}
