<?php

namespace info4c\versioningtool\services;

use Craft;
use craft\base\Component;
use craft\elements\Asset;
use craft\helpers\ArrayHelper;
use craft\helpers\FileHelper;
use craft\helpers\DateTimeHelper;
use info4c\versioningtool\helpers\AuthHelper;
use info4c\versioningtool\models\OriginListModel;
use PhpOffice\PhpSpreadsheet\IOFactory;

use info4c\versioningtool\models\ListModel;
use info4c\versioningtool\records\ListRecord;
use info4c\versioningtool\helpers\Media;

class ReadListService extends Component
{

    protected $listType;
    protected $published;
    protected $mutationsForeign = [];
    protected $mutationsForeignPrev = [];
    protected $newForeign = [];
    protected $deletedForeign = [];
    protected $deletedOrigin = [];
    protected $originList = [];

    /**
     * @param array $file The uploaded authority list
     * @param string $published
     * @param string $listType
     *
     * @return array|bool
     * @throws \Throwable
     * @throws \craft\errors\AssetLogicException
     * @throws \craft\errors\ElementNotFoundException
     * @throws \craft\errors\FileException
     * @throws \yii\base\Exception
     */
    public function storeFile($file, $published, $listType)
    {

        $this->listType = $listType;
        $this->published = $published = DateTimeHelper::toDateTime($published);
        $filename = $published->format('Y-m-d-').FileHelper::sanitizeFilename($file['name']);

        $folder = Media::getListFolder('versionings', $listType);
        $asset = Media::getNewAssetFromFile($file['tmp_name'], $folder, $filename);
        $volume = Craft::$app->getVolumes()->getVolumeById($folder->volumeId);

        $file = Asset::find()
            ->filename($filename)
            ->folderId($folder->id)
            ->one();

        $assets = Craft::$app->getAssets();
        if ($file) {
            $assets->replaceAssetFile($file, $asset->tempFilePath, $asset->filename);
        } else {
            $result = Craft::$app->getElements()->saveElement($asset);
            if ($result) {
                $file = $asset;
            } else {
                return false;
            }
        }

        $filePath = Craft::getAlias($volume->path.'/'.$folder->path.$asset->filename);

        $listRecord = ListRecord::find()
            ->where(['listType' => $listType])
            ->andWhere(['not', ['assetVersioned' => null]])
            ->orderBy('published DESC, dateCreated DESC')
            ->one();

        $prevPublished = '';

        if ($listRecord) {
            $prevPublished = DateTimeHelper::toDateTime($listRecord->getAttribute('published'))->format('d.m.Y');
            // previous authority list
            $previousList = Asset::find()
                ->id($listRecord->getAttribute('assetVersioning'))
                ->one();
            $previousList = Craft::getAlias($volume->path.'/'.$folder->path.$previousList->filename);
            $assetOrigin = Asset::find()
                ->id($listRecord->getAttribute('assetOrigin'))
                ->one();
            $assetOrigin = Craft::getAlias($volume->path.'/'.$folder->path.$assetOrigin->filename);
            $this->createComparisonLists($assetOrigin, $previousList, $filePath);
        } else {
            $this->initList($filePath);
        }

        // store the origin JSON list
        $originFile = Media::writeOriginList($this->originList, $this->listType, $published);
        // get userId by token
        $token = Craft::$app->getRequest()->headers->get('X-Access-Token');
        $userId = AuthHelper::jwtDecode($token);

        $listRecord = new ListRecord;
        $listRecord->setAttribute('userId', $userId);
        $listRecord->setAttribute('listType', $listType);
        $listRecord->setAttribute('assetVersioning', $file->id);
        $listRecord->setAttribute('assetOrigin', $originFile->id);
        $listRecord->setAttribute('published', $published);
        $listRecord->save();

        return [
            'mutationsForeign' => $this->mutationsForeign,
            'mutationsForeignPrev' => $this->mutationsForeignPrev,
            'newForeign' => $this->newForeign,
            'deletedForeign' => $this->deletedForeign,
            'deletedOrigin' => $this->deletedOrigin,
            'published' => $published->format('d.m.Y'),
            'publishedPrevious' => $prevPublished,
            'listUid' => $listRecord->getAttribute('uid'),
        ];
    }

    /**
     * @param $uid
     *
     * @return array
     * @throws \Exception
     */
    public function loadList($uid): array
    {

        $volume = Craft::$app->getVolumes()->getVolumeByHandle('versionings');

        $listRecord = ListRecord::find()
            ->where(['uid' => $uid])
            ->one();
        $this->listType = $listRecord->getAttribute('listType');
        $published = DateTimeHelper::toDateTime($listRecord->getAttribute('published'));

        // data JSON file
        $originAsset = Asset::find()
            ->id($listRecord->getAttribute('assetOrigin'))
            ->one();
        $originFile = Craft::getAlias($volume->path.DIRECTORY_SEPARATOR.$originAsset->path);

        // the authority list
        $currentAsset = Asset::find()
            ->id($listRecord->getAttribute('assetVersioning'))
            ->one();
        $currentFile = Craft::getAlias($volume->path.DIRECTORY_SEPARATOR.$currentAsset->path);

        $prevRecord = ListRecord::find()
            ->where(['listType' => $this->listType])
            ->andWhere(['not', ['id' => $listRecord->getAttribute('id')]])
            ->orderBy('published DESC, dateCreated DESC')
            ->one();

        $prevPublished = '';
        if ($prevRecord) {
            $prevPublished = DateTimeHelper::toDateTime($prevRecord->getAttribute('published'))->format('d.m.Y');
            $prevAsset = Asset::find()
                ->id($prevRecord->getAttribute('assetVersioning'))
                ->one();
            $prevFile = Craft::getAlias($volume->path.DIRECTORY_SEPARATOR.$prevAsset->path);

            $prevOrigin = Asset::find()
                ->id($prevRecord->getAttribute('assetOrigin'))
                ->one();
            $prevOriginFile = Craft::getAlias($volume->path.DIRECTORY_SEPARATOR.$prevOrigin->path);

            $this->createComparisonLists($originFile, $prevFile, $currentFile, $prevOriginFile);
        } else {
            $this->initList($currentFile, $originFile);
        }

        return [
            'mutationsForeign' => $this->mutationsForeign,
            'mutationsForeignPrev' => $this->mutationsForeignPrev,
            'newForeign' => $this->newForeign,
            'deletedForeign' => $this->deletedForeign,
            'deletedOrigin' => $this->deletedOrigin,
            'published' => $published->format('d.m.Y'),
            'publishedPrevious' => $prevPublished,
            'listUid' => $listRecord->getAttribute('uid'),
        ];

    }

    /**
     * @param null $filename
     * @param null $originFile
     *
     * @throws \JsonException
     */
    public function initList($filename = null, $originFile = null): void
    {
        $originList = [];
        if ($originFile) {
            $originTmp = file_get_contents($originFile);
            $originTmp = json_decode($originTmp, true, 512, JSON_THROW_ON_ERROR);
            foreach($originTmp as $row) {
                $originList[$row['strPersonID']] = $row;
            }
        }

        $list = $this->_provListToData($filename);
        $listModel = new OriginListModel($this->listType, $this->published);
        $originAttributes = $listModel->getAttributes();

        $initList = [];
        $strPersonID = 10840000001;
        foreach ($list['data'] as  $hash => $row) {
            if ($originFile) {
                foreach ($originAttributes as $key => $attr) {
                    $row['byInfo4c'][$key] = $originList[$strPersonID][$key];
                }
            } else {
                // fill all fields with default values
                foreach ($originAttributes as $key => $attr) {
                    $row['byInfo4c'][$key] = $attr;
                }
                foreach ($list['info'][$hash] as $key => $attr) {
                    $row['byInfo4c'][$key] = $attr;
                }
                // overwrite the model data
                $row['byInfo4c']['strPersonID'] = $strPersonID;
            }
            $originList[] = $row['byInfo4c'];
            $initList[] = $row;
            ++$strPersonID;
        }
        $this->newForeign = $initList;
        $this->originList = $originList;
    }

    /**
     * @param      $assetOrigin
     * @param      $previousList
     * @param      $currentList
     * @param bool $prevOriginList
     *
     * @throws \JsonException
     */
    public function createComparisonLists($assetOrigin, $previousList, $currentList, $prevOriginList = false): void
    {

        $prevOrigin = [];
        if ($prevOriginList) {
            $prevOrigin = file_get_contents($prevOriginList);
            $prevOrigin = json_decode($prevOrigin, true, 512, JSON_THROW_ON_ERROR);
            $prevOrigin = ArrayHelper::index($prevOrigin, 'dataKey');
        }

        $origin = file_get_contents($assetOrigin);
        $origin = json_decode($origin, true, 512, JSON_THROW_ON_ERROR);
        $origin = ArrayHelper::index($origin, 'dataKey');
        $lastPersonID = 0;
        foreach ($origin as $row) {
            if ($row['strPersonID'] > $lastPersonID) {
                $lastPersonID = $row['strPersonID'];
            }
        }
        // previous authority list
        $listAgainst = $this->_provListToData($previousList);
        // current authority list
        $list = $this->_provListToData($currentList);

        $mutationsForeign  = [];
        $mutationsForeignPrev = [];
        $newForeign = [];
        $deletedForeign = [];
        $deletedOrigin = [];
        $originList = [];

        $listModel = new OriginListModel($this->listType, $this->published);
        $originAttributes = $listModel->getAttributes();

        foreach ($list['data'] as $dataKey => $value) {
            if (isset($listAgainst['data'][$dataKey])) {
                if ($list['info'][$dataKey]['dataHash'] != $listAgainst['info'][$dataKey]['dataHash']) {
                    $mutationsForeignPrev[] = $listAgainst['data'][$dataKey];
                    $byInfo4c = array_merge($origin[$dataKey], $list['info'][$dataKey]);
                    $originList[] = $byInfo4c;
                    $value['byInfo4c'] = $byInfo4c;
                    $mutationsForeign [] = $value;
                } else {
                    $originList[] = $origin[$dataKey];
                }
                unset($listAgainst['data'][$dataKey], $listAgainst['info'][$dataKey]);
            } else if (isset($origin[$dataKey])) {
                $originList[] = $origin[$dataKey];
                $value['byInfo4c'] = $origin[$dataKey];
                $newForeign[] = $value;
            } else {
                ++$lastPersonID;
                $byInfo4c = array_merge($originAttributes, $list['info'][$dataKey]);
                $byInfo4c['strPersonID'] = $lastPersonID;
                $originList[] = $byInfo4c;
                $value['byInfo4c'] = $byInfo4c;
                $newForeign[] = $value;
            }
        }

        foreach ($listAgainst['data'] as $dataKey => $value) {
            $deletedForeign[] = $value;
            if ($prevOriginList) {
                $deletedOrigin[] = $prevOrigin[$dataKey];
            } else {
                $deletedOrigin[] = $origin[$dataKey];
            }
        }

        $this->originList = $originList;
        $this->mutationsForeign = $mutationsForeign;
        $this->mutationsForeignPrev = $mutationsForeignPrev;
        $this->newForeign = $newForeign;
        $this->deletedForeign = $deletedForeign;
        $this->deletedOrigin = $deletedOrigin;

    }

    /**
     * @param $file
     *
     * @return array
     */
    private function _listToData($file): array
    {

        try {
            $spreadsheet = IOFactory::load($file);
            $sheetData = $spreadsheet->getSheet(1)->toArray(null, false, true, false);

            $fieldLabels = [];
            $data = [];

            foreach ($sheetData as $row => $cells) {
                if (empty($fieldLabels) && $cells[0] == 'NOM') {
                    foreach ($cells as $cell) {
                        if ($cell) {
                            $fieldLabels[] = trim($cell);
                        }
                    }
                    continue;
                }
                if (!empty($fieldLabels)) {
                    $cellData = [];
                    foreach($cells as $cell) {
                        if(trim($cell) !== '') {
                            $cellData[] = $cell;
                        }
                    }
                    if (count($cellData) == 0) {
                        continue;
                    }
                    $toPush = [];
                    foreach ($fieldLabels as $key => $label) {
                        $toPush[$label] = trim($cells[$key]);
                    }
                    $toPush['dataHash'] = md5(json_encode($toPush));
                    $search = $toPush['NOM'].$toPush['Prénom'];
                    $search .= $toPush['REGIME DE SANCTION OU DE GEL NATIONAL CONCERNE'];
                    $search .= $toPush['FONDEMENT JURIDIQUE'];
                    $dataKey = md5($search);
                    $toPush['dataKey'] = $dataKey;
                    $data[$dataKey] = $toPush;
                }
            }

            return $data;
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }

    private function _provListToData($file): array
    {

        try {
            $spreadsheet = IOFactory::load($file);
            $sheetData = $spreadsheet->getSheet(1)->toArray(null, false, true, false);

            $fieldLabels = [];
            $listData = [];
            $listInfo = [];

            foreach ($sheetData as $row => $cells) {
                if (empty($fieldLabels) && $cells[0] == 'NOM') {
                    foreach ($cells as $cell) {
                        if ($cell) {
                            $fieldLabels[] = trim($cell);
                        }
                    }
                    continue;
                }
                if (!empty($fieldLabels)) {
                    $cellData = [];
                    foreach($cells as $cell) {
                        if(trim($cell) !== '') {
                            $cellData[] = $cell;
                        }
                    }
                    if (count($cellData) == 0) {
                        continue;
                    }
                    $toPush = [];
                    $toPushInfo = [];
                    foreach ($fieldLabels as $key => $label) {
                        $toPush[$label] = trim($cells[$key]);
                    }
                    $toPushInfo['dataHash'] = md5(json_encode($toPush));
                    $search = $toPush['NOM'].$toPush['Prénom'];
                    $search .= $toPush['REGIME DE SANCTION OU DE GEL NATIONAL CONCERNE'];
                    $search .= $toPush['FONDEMENT JURIDIQUE'];
                    $dataKey = md5($search);
                    $toPushInfo['dataKey'] = $dataKey;
                    $listData[$dataKey] = $toPush;
                    $listInfo[$dataKey] = $toPushInfo;
                }
            }

            return ['data' => $listData, 'info' => $listInfo];
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }
}
