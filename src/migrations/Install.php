<?php

namespace info4c\versioningtool\migrations;

use Craft;
use craft\db\Migration;
use info4c\versioningtool\records\ListRecord;

/**
 * Install migration.
 */
class Install extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableName = ListRecord::tableName();
        if (!$this->db->tableExists($tableName)) {
            $this->createTable($tableName, [
                'id' => $this->primaryKey(),
                'userId' => $this->integer()->null(),
                'listType' => $this->string(255)->notNull(),
                'assetVersioning' => $this->integer()->null(),
                'assetVersioned' => $this->integer()->null(),
                'assetOrigin' => $this->integer()->null(),
                'published' => $this->dateTime()->notNull(),
                'dateCreated' => $this->dateTime()->notNull(),
                'dateUpdated' => $this->dateTime()->notNull(),
                'uid' => $this->uid(),
            ]);

            $this->createIndex(null, $tableName, 'userId', false);
            $this->createIndex(null, $tableName, 'assetVersioning', false);
            $this->createIndex(null, $tableName, 'assetVersioned', false);
            $this->createIndex(null, $tableName, 'assetOrigin', false);
            $this->addForeignKey(null, $tableName, 'userId', '{{%elements}}', 'id', 'SET NULL', null);
            $this->addForeignKey(null, $tableName, 'assetVersioning', '{{%elements}}', 'id', 'SET NULL', null);
            $this->addForeignKey(null, $tableName, 'assetVersioned', '{{%elements}}', 'id', 'SET NULL', null);
            $this->addForeignKey(null, $tableName, 'assetOrigin', '{{%elements}}', 'id', 'SET NULL', null);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $tableName = ListRecord::tableName();
        $this->dropTableIfExists($tableName);
    }
}
