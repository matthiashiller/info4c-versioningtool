<?php

namespace info4c\versioningtool\helpers;

use PhpOffice\PhpSpreadsheet\IOFactory;

class SpreadsheetHelper
{

    /**
     * @param $file
     * @param $activeSheet
     * @param $firstKey
     *
     * @return array
     */
    public static function spreadsheetToArray($file, $activeSheet, $firstKey): array
    {
        try {
            $spreadsheet = IOFactory::load($file);
            $sheetData = $spreadsheet->getSheet($activeSheet)->toArray(null, false, true, false);

            $fieldLabels = [];
            $data = [];
            foreach ($sheetData as $row => $cells) {
                if (empty($fieldLabels) && $cells[0] == $firstKey) {
                    foreach ($cells as $cell) {
                        if ($cell) {
                            $fieldLabels[] = trim($cell);
                        }
                    }
                    continue;
                }
                if (!empty($fieldLabels)) {
                    $toPush = [];
                    foreach ($fieldLabels as $key => $label) {
                        $toPush[$label] = trim($cells[$key]);
                    }
                    $data[] = $toPush;
                }
            }

            return $data;
        } catch (\Exception $e) {
            return ['message' => $e->getMessage()];
        }
    }

    /**
     * @param $file
     *
     * @return array|bool
     */
    public static function checkAssetVersioned($file)
    {

        $bluePrint = [
            'strPersonID', 'strTitle', 'strFirstName', 'strLastName',
            'strFullName', 'memOtherNames', 'strAlternativeScript',
            'strDOB', 'strPOB', 'strType_SDN_or_Entity', 'strCountry',
            'strNotes', 'memAddress', 'strPassportNr', 'strNameOfTheList',
            'strTypeOfList', 'strDateOfPublicationOfTheList', 'strAuthority',
            'strWhitelist', 'memAdditionalInformation'
        ];

        try {
            $colsExists = true;
            $spreadsheet = IOFactory::load($file['tmp_name']);
            $worksheet = $spreadsheet->getSheet(0);
            for ($col = 1, $colMax = count($bluePrint); $col <= $colMax; ++$col) {
                $cell = $worksheet->getCellByColumnAndRow($col, 1);
                if ($cell && in_array(trim($cell->getValue()), $bluePrint, true)) {
                    continue;
                }
                $colsExists = false;
            }
            return $colsExists;
        } catch (\Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }

    public static function checkAssetVersioning($file)
    {
        $bluePrint = [
            'NOM', 'Prénom', 'Alias', 'Date de naissance', 'Lieu de naissance',
            'Autres informations', 'NATURE',
            'REGIME DE SANCTION OU DE GEL NATIONAL CONCERNE',
            'FONDEMENT JURIDIQUE', 'ARRETE D\'EXTENSION DE LA MESURE AUX PTOM',
            'LIEN UTILE', 'DATE D\'AJOUT AU REGISTRE', 'ENSEMBLE DES INFORMATIONS',
        ];
        try {
            $colsExists = true;
            $spreadsheet = IOFactory::load($file['tmp_name']);
            $worksheet = $spreadsheet->getSheet(1);
            for ($col = 1, $colMax = count($bluePrint); $col <= $colMax; ++$col) {
                $cell = $worksheet->getCellByColumnAndRow($col, 3);
                if ($cell && in_array(trim($cell->getValue()), $bluePrint, true)) {
                    continue;
                }
                $colsExists = false;
            }
            return $colsExists;
        } catch (\Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }
}
