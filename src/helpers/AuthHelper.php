<?php

namespace info4c\versioningtool\helpers;

use Craft;
use craft\elements\User;
use craft\helpers\StringHelper;
use craft\helpers\Template;
use craft\mail\Message;
use craft\web\View;
use ReallySimpleJWT\Encode;
use ReallySimpleJWT\Exception\ValidateException;
use ReallySimpleJWT\Jwt;
use ReallySimpleJWT\Parse;
use ReallySimpleJWT\Token;
use ReallySimpleJWT\Validate;
use yii\base\Exception;

class AuthHelper
{

    /**
     * @param $userId
     * @param $expiration
     *
     * @return string
     */
    public static function jwtEncode($userId, $expiration): string
    {
        $issuer = Craft::getAlias('@web');
        return Token::create($userId, self::getJwtSecret(), $expiration, $issuer);
    }

    /**
     * @param $token
     *
     * @return array|mixed
     */
    public static function jwtDecode($token)
    {
        $jwt = new Jwt($token, self::getJwtSecret());
        $parse = new Parse($jwt, new Validate(), new Encode());
        try {
            $parsed = $parse->validate()
                ->validateExpiration()
                ->parse();
            $payload = $parsed->getPayload();
            return $payload['user_id'];
        } catch (ValidateException $e) {
            return ['message' => $e->getMessage()];
        }
    }

    public static function userHasToolPermission($userId)
    {
        $allPermissions = Craft::$app->getUserPermissions()->getPermissionsByUserId($userId);
        $permissions = array_filter($allPermissions, static function($key) {
            return strpos($key, 'versioningtool:') === 0;
        });

        return count($permissions) > 0;
    }

    /**
     * @param $token
     * @param $listType
     *
     * @return bool
     */
    public static function userHasPermission($token, $listType): bool
    {
        $userId = self::jwtDecode($token);
        $user = Craft::$app->getUsers()->getUserById($userId);
        if ($user && $user->admin) {
            return true;
        }
        $handle = 'versioningtool:' . $listType;

        return Craft::$app->getUserPermissions()->doesUserHavePermission($userId, $handle);
    }

    /**
     * @return array|false|string
     */
    public static function getJwtSecret()
    {
        return getenv('JWT_SECRET');
    }

    /**
     * @param User $user
     *
     * @return array
     */
    public static function navigationByUser(User $user): array
    {
        $userPermissions = Craft::$app->getUserPermissions()->getAssignablePermissions($user);

        $navs = [];
        foreach($userPermissions['Versioning Tool'] as  $country) {
            foreach($country['nested'] as $key => $list) {
                $navs[$country['label']][] = [
                    'label' => $list['label'],
                    'list' => explode(':', $key)[1],
                ];
            }
        }

        return $navs;
    }

    /**
     * @param $user
     *
     * @return bool
     * @throws Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public static function sendResetPassword($user)
    {
        $users = Craft::$app->getUsers();
        $pwResetUrl = $users->getPasswordResetUrl($user);
        $origin = Craft::$app->getRequest()->getOrigin();
        $urlQuery = explode('?', $pwResetUrl)[1];
        $pwResetUrl = Template::raw($origin.'/set-password?'.$urlQuery);

        $oldMode = \Craft::$app->view->getTemplateMode();
        Craft::$app->view->setTemplateMode(View::TEMPLATE_MODE_CP);
        $mailHtml = Craft::$app->view->renderTemplate('versioning-tool/_email/reset-password-html', ['link' => $pwResetUrl, 'user' => $user]);
        $mailText = Craft::$app->view->renderTemplate('versioning-tool/_email/reset-password-text', ['link' => $pwResetUrl, 'user' => $user]);
        Craft::$app->view->setTemplateMode($oldMode);


        $settings = Craft::$app->projectConfig->get('email');
        $message = new Message();
        $message->setFrom([getenv('SYSTEM_EMAIL_ADDRESS') => getenv('SYSTEM_SENDER_NAME')]);
        $message->setTo($user->email);
        $message->setSubject('reset password');
        $message->setHtmlBody($mailHtml);
        $message->setTextBody($mailText);
        return Craft::$app->mailer->send($message);

    }

}
