<?php

namespace info4c\versioningtool\helpers;

use Craft;
use craft\elements\Asset;
use craft\helpers\FileHelper;
use craft\models\VolumeFolder;

class Media
{

    /**
     * @param $volumeHandler
     * @param $listType
     *
     * @return VolumeFolder|null
     */
    public static function getListFolder($volumeHandler, $listType): ?VolumeFolder
    {
        switch ($listType) {
            case 'frenchFreezeList':
                $path = 'france/freeze-lists/';
                break;
            default:
                $path = 'france/freeze-lists/';
        }
        $volume = Craft::$app->getVolumes()->getVolumeByHandle($volumeHandler);
        $folderQuery = ['path' => $path, 'volumeId' => $volume->id];

        $assets = Craft::$app->getAssets();
        return $assets->findFolder($folderQuery);
    }

    /**
     * @param      $file
     * @param      $folder
     * @param null $fileName
     *
     * @return Asset
     */
    public static function getNewAssetFromFile($file, $folder, $fileName = null): Asset
    {
        $asset = new Asset();
        $asset->tempFilePath = $file;
        $asset->filename = $fileName ?? basename($file);
        $asset->newFolderId = $folder->id;
        $asset->volumeId = $folder->volumeId;
        $asset->avoidFilenameConflicts = true;
        $asset->setScenario(Asset::SCENARIO_CREATE);

        return $asset;
    }

    /**
     * @param $list
     * @param $listType
     * @param $publishDate
     *
     * @return Asset
     * @throws \JsonException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     */
    public static function writeOriginList($list, $listType, $publishDate): Asset
    {
        $tempPath  = Craft::$app->getPath()->getTempPath();
        $originFile = $tempPath.DIRECTORY_SEPARATOR.$publishDate->format('Y-m-d-').'origin.json';
        FileHelper::writeToFile($originFile, json_encode($list, JSON_THROW_ON_ERROR, 512));
        $folder = self::getListFolder('versionings', $listType);
        $asset = self::getNewAssetFromFile($originFile, $folder);
        Craft::$app->getElements()->saveElement($asset);
        return $asset;
    }

}
