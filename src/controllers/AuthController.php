<?php

namespace info4c\versioningtool\controllers;

use craft\elements\User;
use info4c\versioningtool\helpers\AuthHelper;

use Craft;

use yii\web\HttpException;
use yii\web\Response;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;

class AuthController extends ToolController
{
    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = [
        'login',
        'renew-token',
        'forgot-password',
        'set-password',
    ];

    /**
     * @return bool|Response
     * @throws ForbiddenHttpException
     */
    public function actionLogin()
    {

        $request = Craft::$app->request;
        $method = $request->getMethod();
        if ($method == 'POST') {
            $loginName = $request->getBodyParam('loginName');
            $password = $request->getBodyParam('password');
            if (!$loginName) {
                throw new ForbiddenHttpException('Missing credentials!');
            }
            $user = Craft::$app->getUsers()->getUserByUsernameOrEmail($loginName);
            if (!$user || !$user->authenticate($password)) {
                throw new ForbiddenHttpException('Invalid credentials!');
            }
            return $this->_sendLoginResponse($user);
        }
        return true;
    }

    /**
     * @return bool|Response
     */
    public function actionRenewToken()
    {
        $request = Craft::$app->getRequest();
        $token = $request->headers->get('X-Access-Token');
        if ($token) {
            $userId = AuthHelper::jwtDecode($token);
            $expiration = time() + 3600;
            $newToken = AuthHelper::jwtEncode($userId, $expiration);
            return $this->asJson([
                'token' => $newToken,
                'expiration' => $expiration,
            ]);
        }
        return true;
    }


    /**
     * @return bool|Response
     * @throws ForbiddenHttpException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \yii\base\Exception
     */
    public function actionForgotPassword()
    {
        $request = Craft::$app->getRequest();
        $method = $request->getMethod();
        if ($method == 'POST') {
            $loginName = $request->getBodyParam('loginName');
            if (!$loginName) {
                throw new ForbiddenHttpException('Missing email/username!');
            }
            $user = Craft::$app->getUsers()->getUserByUsernameOrEmail($loginName);
            if (!$user) {
                throw new ForbiddenHttpException('Error occurred');
            }

            $output = [
                'sent' => AuthHelper::sendResetPassword($user),
                'user' => [
                    'id' => $user->id,
                    'email' => $user->email,
                ]
            ];
            return $this->asJson($output);
        }
        return true;
    }

    /**
     * @return bool|Response
     * @throws BadRequestHttpException
     * @throws HttpException
     * @throws \Throwable
     * @throws \craft\errors\ElementNotFoundException
     * @throws \yii\base\Exception
     */
    public function actionSetPassword()
    {
        $request = Craft::$app->getRequest();
        $method = $request->getMethod();
        if ($method == 'POST') {
            $users = Craft::$app->getUsers();
            $user = $users->getUserByUid($request->getBodyParam('id'));
            $validCode = $users->isVerificationCodeValidForUser($user, $request->getBodyParam('code'));
            if (!$user || !$validCode) {
                throw new HttpException('403', Craft::t('app', 'Invalid verification code. Please login or reset your password.'));
            }
            $user->newPassword = $request->getRequiredBodyParam('newPassword');
            $user->setScenario(User::SCENARIO_PASSWORD);
            if (!Craft::$app->getElements()->saveElement($user)) {
                Craft::$app->getResponse()->setStatusCode(400);
                $errors = $user->getErrors('newPassword');
                return $this->asErrorJson(implode(', ', $errors));
            }
            return $this->_sendLoginResponse($user);
        }
        return true;
    }

    /**
     * @param $user
     *
     * @return Response
     */
    private function _sendLoginResponse($user): Response
    {

        $toolPermission = $user->admin || AuthHelper::userHasToolPermission($user->id);
        if (!$toolPermission) {
            throw new ForbiddenHttpException('You have no permission for this tool!');
        }
        $expiration = time() + 3600;
        $token = AuthHelper::jwtEncode($user->id, $expiration);
        $navs = AuthHelper::navigationByUser($user);
        $output = [
            'user' => [
                'email' => $user->email,
                'loginName' => $user->username,
                'friendlyName' => $user->friendlyName,
                'uid' => $user->uid,
            ],
            'token' => $token,
            'expiration' => $expiration,
            'navs' => $navs,
        ];
        return $this->asJson($output);
    }
}
