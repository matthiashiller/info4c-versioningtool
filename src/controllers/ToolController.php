<?php

namespace info4c\versioningtool\controllers;

use Craft;
use craft\web\Controller;
use info4c\versioningtool\records\ListRecord;
use yii\filters\Cors;
use yii\web\ForbiddenHttpException;

use info4c\versioningtool\helpers\AuthHelper;

class ToolController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * @return array
     */

    public function behaviors(): array
    {

        $request = Craft::$app->getRequest();
        $origin = $request->getOrigin();

        return array_merge(parent::behaviors(), [
            'corsFilter' => [
                'class' => Cors::class,
                'cors' => [
                    'Origin' => [$origin],
                    'Access-Control-Request-Method' => ['POST', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['Content-Type', 'X-Access-Token'],
                ],
            ],
        ]);
    }

    /**
     * @param \yii\base\Action $action
     *
     * @return bool|\yii\web\Response
     * @throws \craft\web\ServiceUnavailableHttpException
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function beforeAction($action)
    {
        parent::beforeAction($action);

        $request = Craft::$app->getRequest();
        $method = $request->getMethod();
        if ($method == 'POST') {
            $this->requirePostRequest();
            $this->requireAcceptsJson();
        }

        return true;
    }

    /**
     * @param \yii\base\Action $action
     * @param mixed            $result
     *
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        $request = Craft::$app->getRequest();
        $method = $request->getMethod();
        if ($method == 'POST') {
            $controller = $action->controller->id;
            if ($controller != 'auth') {
                $token = $request->headers->get('X-ACCESS-TOKEN');
                $listType = $request->getBodyParam('listType');
                $listUid = $request->getBodyParam('listUid');
                if (!$listType && $listUid) {
                    $listRecord = ListRecord::find()
                        ->where(['uid' => $listUid])
                        ->one();
                    if ($listRecord) {
                        $listType = $listRecord->listType;
                    }
                }
                if ($listType) {
                    $permission = AuthHelper::userHasPermission($token, $listType);
                    if (!$permission) {
                        throw new ForbiddenHttpException('access denied');
                    }
                }
            }
        }
        return $result;
    }

}
