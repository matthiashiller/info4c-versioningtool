<?php

namespace info4c\versioningtool\controllers;

use craft\elements\Asset;
use info4c\versioningtool\models\OriginListModel;
use info4c\versioningtool\VersioningTool;
use info4c\versioningtool\helpers\SpreadsheetHelper;

use Craft;
use yii\base\InvalidConfigException;
use yii\filters\Cors;
use yii\web\BadRequestHttpException;
use yii\web\Response;

class ListController extends ToolController
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = [
        'list-status',
        'upload-list',
        'create-list',
        'load-list',
        'replace-list',
        'reset-list',
        'get-asset',
    ];
    public $enableCsrfValidation = false;

    // Public Methods
    // =========================================================================

    /**
     * @return bool|Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionListStatus()
    {
        $request = Craft::$app->getRequest();
        $listType = $request->getBodyParam('listType');
        if ($listType) {
            $status = VersioningTool::$plugin->list->listStatus($listType);
            return $this->asJson($status);
        }
        return true;
    }

    /**
     * @return bool|Response
     * @throws \Throwable
     * @throws \craft\errors\AssetLogicException
     * @throws \craft\errors\ElementNotFoundException
     * @throws \craft\errors\FileException
     * @throws \yii\base\Exception
     */
    public function actionUploadList()
    {
        $request = Craft::$app->getRequest();
        $listType = $request->getBodyParam('listType');
        if ($listType) {
            $file = $_FILES['list'];
            $test = SpreadsheetHelper::checkAssetVersioning($file);
            if (!$test) {
                $response = Craft::$app->getResponse();
                $response->setStatusCode(400);
                return $this->asJson(['error' => 'wrong file']);
            }
            $published = $request->getBodyParam('published');
            $list = VersioningTool::$plugin->readList->storeFile($file, $published, $listType);
            $fields = new OriginListModel();
            $originFields = $fields->getAttributes();
            return $this->asJson(array_merge($list, ['originFields' => $originFields]));
        }
        return true;
    }


    /**
     * @return bool|Response
     * @throws InvalidConfigException
     * @throws \Throwable
     * @throws \craft\errors\ElementNotFoundException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     */
    public function actionCreateList()
    {
        $request = Craft::$app->request;
        $listType = $request->getBodyParam('listType');
        if ($listType) {
            $params = $request->getBodyParams();
            $list = VersioningTool::$plugin->createList->createList($params);
            return $this->asJson($list);
        }
        return true;
    }

    /**
     * @return bool|Response
     * @throws \Exception
     */
    public function actionLoadList()
    {
        $request = Craft::$app->getRequest();
        $uid = $request->getBodyParam('listUid');
        if ($uid) {
            $list = VersioningTool::$plugin->readList->loadList($uid);
            $fields = new OriginListModel();
            $originFields = $fields->getAttributes();
            return $this->asJson(array_merge($list, ['originFields' => $originFields]));
        }
        return true;
    }

    /**
     * @return bool|Response
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \craft\errors\AssetLogicException
     * @throws \craft\errors\FileException
     */
    public function actionReplaceList()
    {
        $request = Craft::$app->getRequest();
        $uid = $request->getBodyParam('listUid');
        if ($uid) {
            $file = $_FILES['listReplace'];
            $test = SpreadsheetHelper::checkAssetVersioned($file);
            if (!$test) {
                $response = Craft::$app->getResponse();
                $response->setStatusCode(400);
                return $this->asJson(['error' => 'wrong file']);
            }
            $state = VersioningTool::$plugin->list->replaceListAsset($file, $uid);
            return $this->asJson($state);
        }
        return true;
    }

    /**
     * @return bool|Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionResetList()
    {
        $request = Craft::$app->getRequest();
        $uid = $request->getBodyParam('listUid');
        if ($uid) {
            $status = VersioningTool::$plugin->list->resetList($uid);
            return $this->asJson($status);
        }
        return true;
    }

    /**
     * @param $uid
     */
    public function actionGetAsset($uid)
    {
        $asset = Asset::find()
            ->uid($uid)
            ->one();
        $volumePath = Craft::$app->getVolumes()->getVolumeById($asset->volumeId)->path;
        $file = Craft::getAlias($volumePath.'/'.$asset->path);
        Craft::$app->getResponse()->sendFile($file);
    }

}
