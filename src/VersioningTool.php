<?php
namespace info4c\versioningtool;

use craft\base\Plugin;
use craft\events\RegisterUrlRulesEvent;
use craft\web\twig\variables\CraftVariable;
use craft\events\RegisterUserPermissionsEvent;
use craft\services\UserPermissions;
use craft\web\UrlManager;

use info4c\versioningtool\services\ListService;
use info4c\versioningtool\services\ReadListService;
use info4c\versioningtool\services\CreateListService;
use info4c\versioningtool\variables\VersioningToolVariable;

use yii\base\Event;

/**
 * Class VersioningTool
 *
 * @property ListService $list
 * @property ReadListService $readList
 * @property CreateListService $createList
 */
class VersioningTool extends Plugin
{

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '1.0.0';

    // Static Properties
    // =========================================================================

    /**
     * @var VersioningTool
     */
    public static $plugin;

    // Public Methods
    // =========================================================================

    public function init()
    {
        parent::init();

        self::$plugin = $this;

        $this->setComponents([
            'list' => ListService::class,
            'createList' => CreateListService::class,
            'readList' => ReadListService::class,
        ]);

        Event::on(
            UserPermissions::class,
            UserPermissions::EVENT_REGISTER_PERMISSIONS,
            static function(RegisterUserPermissionsEvent $event) {
                $event->permissions['Versioning Tool'] = [
                    'versioningtool:french' => [
                        'label' => 'France',
                        'nested' => [
                            'versioningtool:frenchFreezeList' => [
                                'label' => 'Freeze List',
                            ],
                        ],
                    ],
                ];
            }
        );

        Event::on(CraftVariable::class, CraftVariable::EVENT_INIT, static function(Event $event) {
            /** @var CraftVariable $variable */
            $variable = $event->sender;
            $variable->set('versioningTool', VersioningToolVariable::class);
        });

        Event::on(UrlManager::class, UrlManager::EVENT_REGISTER_SITE_URL_RULES, static function(RegisterUrlRulesEvent $event) {
            $event->rules['versioning-asset/<uid:[A-Za-z0-9]{8}-[A-Za-z0-9]{4}-4[A-Za-z0-9]{3}-[89abAB][A-Za-z0-9]{3}-[A-Za-z0-9]{12}>'] = 'versioning-tool/list/get-asset';
        });

    }
}
