<?php

namespace info4c\versioningtool\variables;

use info4c\versioningtool\VersioningTool;
use const http\Client\Curl\VERSIONS;

class VersioningToolVariable
{
    public function frenchList()
    {
        return VersioningTool::$plugin->readList->frenchList();
    }

    /**
     * @return VersioningTool
     */
    public function testing()
    {
        return VersioningTool::$plugin;
    }
}
