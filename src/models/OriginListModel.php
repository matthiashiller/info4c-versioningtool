<?php

namespace info4c\versioningtool\models;

use craft\base\Model;

class OriginListModel extends Model
{

    /**
     * @var int
     */
    public $strPersonID;

    /**
     * @var string
     */
    public $strTitle = '-0-';

    /**
     * @var string
     */
    public $strFirstName = '-0-';

    /**
     * @var string
     */
    public $strLastName = '-0-';

    /**
     * @var string
     */
    public $strFullName = '-0-';

    /**
     * @var string
     */
    public $memOtherNames = '-0-';

    /**
     * @var string
     */
    public $strAlternativeScript = '-0-';

    /**
     * @var string
     */
    public $strDOB = '-0-';

    /**
     * @var string
     */
    public $strPOB = '-0-';

    /**
     * @var string
     */
    public $memAdditionalInformation = '-0-';

    /**
     * @var string
     */
    public $strType_SDN_or_Entity = '-0-';

    /**
     * @var string
     */
    public $strCountry = '-0-';

    /**
     * @var string
     */
    public $strNotes = '-0-';

    /**
     * @var string
     */
    public $memAddress = '-0-';

    /**
     * @var string
     */
    public $strPassportNr = '-0-';

    /**
     * @var string
     */
    public $strNameOfTheList = '-0-';

    /**
     * @var string
     */
    public $strTypeOfList = '-0-';

    /**
     * @var string
     */
    public $strDateOfPublicationOfTheList = '-0-';

    /**
     * @var string
     */
    public $strAuthority = '-0-';

    /**
     * @var string
     */
    public $strWhitelist = "FALSE";

    public function __construct($listType = null, $published = null, $config = [])
    {
        parent::__construct($config);
        if ($listType) {
            $this->_defaultFieldValues($listType, $published);
        }
        if ($published) {
            $this->strDateOfPublicationOfTheList = $published->format('d.m.Y');
        }
    }

    public function __toString()
    {
        return (string)$this->strFullName;
    }

    private function _defaultFieldValues($listType, $published): void
    {
        switch ($listType) {
            case 'frenchFreezeList':
                $this->strNameOfTheList = 'List of persons subject to national freezing of assets';
                $this->strTypeOfList = 'France List';
                $this->strAuthority = 'Direction générale du Trésor';
        }
    }

}
