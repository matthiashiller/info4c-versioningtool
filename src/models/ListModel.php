<?php

namespace info4c\versioningtool\models;

use craft\base\Model;
use DateTime;

class ListModel extends Model
{
    /**
     * @var int|null ID
     */
    public $id;

    /**
     * $userId int|null ID
     */
    public $userId;

    /**
     * @var string List type
     */
    public $listType;

    /**
     * @var int|null File for versioning
     */
    public $assetVersioning;

    /**
     * @var int|null File of versioned list
     */
    public $assetVersioned;

    /**
     * @var int|null File of origin info4d list
     */
    public $assetOrigin;

    /**
     * @var DateTime|null Date of publication authority list
     */
    public $published;

    /**
     * @var DateTime|null Date created
     */
    public $dateCreated;

    /**
     * @var DateTime|null Date updated
     */
    public $dateUpdated;

    public function __toString()
    {
        return (string)$this->listType;
    }
}
