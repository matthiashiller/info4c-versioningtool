<?php

namespace info4c\versioningtool\records;

use craft\db\ActiveRecord;

/**
 * Class ListRecord
 *
 * @package      info4c\versioningtool\records
 *
 * @property int $id               ID
 * @property int $userId           User ID
 * @property int $assetVersioning  Asset ID for versioning
 * @property int $assetVersioned   Asset ID for versioned list
 * @property int $assetOrigin      Asset ID for origin info4d list
 */
class ListRecord extends ActiveRecord
{
    // Public Static Methods
    // =========================================================================

    /**
     * @inheritdoc
     *
     * @return string the table name
     */
    public static function tableName(): string
    {
        return '{{%info4c_lists}}';
    }
}
